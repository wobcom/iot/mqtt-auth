// nolint:gochecknoglobals,gochecknoinits,exhaustivestruct
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print version",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("mqtt-auth %s, commit %s, %s", version, commit, date) //nolint:forbidigo
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
