//nolint: gochecknoglobals,gochecknoinits,gomnd,exhaustivestruct
package cmd

import (
	"bytes"
	"io/ioutil"
	"os"
	"reflect"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/config"
)

var cfgFile string

var rootCmd = &cobra.Command{
	Use:   "auth-service",
	Short: "Auth Service",
	RunE:  run,
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "path to configuration file (optional)")
	rootCmd.PersistentFlags().Int("log-level", 4, "debug=5, info=4, error=2, fatal=1, panic=0")

	// bind flag to config vars
	err := viper.BindPFlag("general.log_level", rootCmd.PersistentFlags().Lookup("log-level"))
	if err != nil {
		log.Fatal().Msg(err.Error())
	}
	// defaults
	viper.SetDefault("general.shutdown_timeout", 5*time.Second)
	viper.SetDefault("general.strict_mode", true)
	viper.SetDefault("general.enable_mqtt_v5", false)
	viper.SetDefault("general.enable_playground", false)
	viper.SetDefault("auth.enable", true)
	viper.SetDefault("postgresql.dsn", "postgres://localhost/vernemq_db?sslmode=disable&connect_timeout=5")
	viper.SetDefault("postgresql.max_open_connections", 5)
	viper.SetDefault("postgresql.max_idle_connections", 2)
	viper.SetDefault("postgresql.max_conn_lifetime", 5*time.Second)
	viper.SetDefault("postgresql.auto_migrate", true)

	rootCmd.AddCommand(configCmd)
}

// Execute executes the root command.
func Execute() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	if err := rootCmd.Execute(); err != nil {
		log.Fatal().Msg(err.Error())
	}
}

func initConfig() {
	if cfgFile != "" {
		handleConfigPath(cfgFile)
	} else {
		handleNoConfigPath()
	}

	handleEnvsWithDots()

	viperBindEnvs(config.C)

	if err := viper.Unmarshal(&config.C); err != nil {
		log.Fatal().Err(err).Msg("unmarshal config error")
	}
}

func viperBindEnvs(iface interface{}, parts ...string) {
	ifv := reflect.ValueOf(iface)
	ift := reflect.TypeOf(iface)

	for i := 0; i < ift.NumField(); i++ {
		v := ifv.Field(i)
		t := ift.Field(i)

		tv, ok := t.Tag.Lookup("mapstructure")
		if !ok {
			tv = strings.ToLower(t.Name)
		}

		if tv == "-" {
			continue
		}

		// nolint: exhaustive
		switch v.Kind() {
		case reflect.Struct:
			viperBindEnvs(v.Interface(), append(parts, tv)...)
		default:
			// Bash doesn't allow env variable names with a dot so
			// bind the double underscore version.
			keyDot := strings.Join(append(parts, tv), ".")
			keyUnderscore := strings.Join(append(parts, tv), "__")

			err := viper.BindEnv(keyDot, strings.ToUpper(keyUnderscore))
			if err != nil {
				log.Fatal().Msg(err.Error())
			}
		}
	}
}

func handleConfigPath(cfgFile string) {
	b, err := ioutil.ReadFile(cfgFile)
	if err != nil {
		log.Error().Err(err).Str("config", cfgFile).Msg("error loading config file")
	}

	viper.SetConfigType("toml")

	if err := viper.ReadConfig(bytes.NewBuffer(b)); err != nil {
		log.Error().Err(err).Str("config", cfgFile).Msg("error loading config file")
	}
}

func handleNoConfigPath() {
	viper.SetConfigName("auth-service")
	viper.AddConfigPath("./")
	viper.AddConfigPath("$HOME/.config/auth-service")
	viper.AddConfigPath("/etc/auth-service")

	if err := viper.ReadInConfig(); err != nil {
		switch err.(type) { //nolint:errorlint
		case viper.ConfigFileNotFoundError:
			log.Warn().Msg("No configuration file found, using defaults")
		default:
			log.Fatal().Err(err).Msg("read configuration file error")
		}
	}
}

func handleEnvsWithDots() {
	for _, pair := range os.Environ() {
		d := strings.SplitN(pair, "=", 2)
		if strings.Contains(d[0], ".") {
			log.Warn().
				Msgf("Using dots in env variable is illegal and deprecated. Please use double underscore `__` for: %s", d[0])

			underscoreName := strings.ReplaceAll(d[0], ".", "__")
			// Set only when the underscore version doesn't already exist.
			if _, exists := os.LookupEnv(underscoreName); !exists {
				os.Setenv(underscoreName, d[1])
			}
		}
	}
}
