// nolint: gochecknoglobals,exhaustivestruct
package cmd

import (
	"os"
	"text/template"

	"github.com/pkg/errors"
	"github.com/spf13/cobra"
	"gitlab.com/wobcom/iot/mqtt-auth/assets"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/config"
)

var configCmd = &cobra.Command{
	Use:   "configfile",
	Short: "Print the Auth-Service configuration file",
	RunE: func(cmd *cobra.Command, args []string) error {
		t := template.Must(template.New("config").Parse(assets.ConfigTpl))
		err := t.Execute(os.Stdout, config.C)
		if err != nil {
			return errors.Wrap(err, "execute config template error")
		}

		return nil
	},
}
