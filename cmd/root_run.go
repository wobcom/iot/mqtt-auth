//nolint:exhaustivestruct
package cmd

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/MicahParks/keyfunc"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/api/auth"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/api/gql"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/api/vernemq"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/config"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/storage"
)

func run(cmd *cobra.Command, args []string) error {
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	log.Info().Msg("Starting Auth-Service")

	err := storage.Setup(config.C)
	if err != nil {
		panic(err)
	}

	log.Info().Msg("Storage initialized")

	extSrv, _ := setupExternalAPI()
	intSrv := setupInternalAPI()

	// defer jwks.EndBackground()

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if err := extSrv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatal().Msgf("listen: %s\n", err)
		}
	}()

	go func() {
		if err := intSrv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatal().Msgf("listen: %s\n", err)
		}
	}()

	log.Print("Server Started")

	<-done
	log.Print("Server Stopped")

	ctx, cancel := context.WithTimeout(context.Background(), config.C.General.ShutdownTimeout)
	defer cancel()

	if err := extSrv.Shutdown(ctx); err != nil {
		log.Panic().Msgf("Server Shutdown Failed:%+v", err)
	}

	if err := intSrv.Shutdown(ctx); err != nil {
		log.Panic().Msgf("Server Shutdown Failed:%+v", err)
	}

	log.Info().Msg("Server Exited Properly")

	return nil
}

func setupExternalAPI() (*http.Server, *keyfunc.JWKs) {
	router := mux.NewRouter()
	apiRouter := router.NewRoute().Subrouter()

	jwks, err := auth.SetupJWKS(config.C)
	if err != nil {
		log.Fatal().Msgf("Failed to setup JWKS \nError: %s", err.Error())
	}

	ctx := context.Background()

	provider, err := auth.NetOAuthProvider(ctx, config.C)
	if err != nil {
		log.Fatal().Msgf("Failed to setup OAuth Provider \nError: %s", err.Error())
	}

	oauthConfig := auth.NewOAuth2Config(config.C, provider)

	if err := auth.Setup(config.C, provider, oauthConfig, router); err != nil {
		log.Fatal().Msgf("Failed to setup OIDC \nError: %s", err.Error())
	}

	if err := auth.SetupCORS(config.C, apiRouter); err != nil {
		log.Fatal().Msgf("Failed to setup CORS \nError: %s", err.Error())
	}

	if err := auth.SetupProtectedRouter(jwks, config.C, oauthConfig, apiRouter); err != nil {
		log.Fatal().Msgf("Failed to setup Protected Router \nError: %s", err.Error())
	}

	if config.C.General.PlaygroundEnabled {
		router.Handle("/api/playground", playground.Handler("GraphQL playground", "/api/query"))
	}

	if err := gql.SetupQuery(config.C, apiRouter); err != nil {
		log.Fatal().Msgf("Failed to setup GraphQL Query Endpoint \nError: %s", err.Error())
	}

	if err := gql.SetupPlayground(config.C, router); err != nil {
		log.Fatal().Msgf("Failed to setup GraphQL Playground Endpoint \nError: %s", err.Error())
	}

	return &http.Server{
		Addr:    ":8080",
		Handler: router,
	}, jwks
}

func setupInternalAPI() *http.Server {
	router := mux.NewRouter()
	if err := vernemq.Setup(config.C, router); err != nil {
		log.Fatal().Msgf("Failed to setup VerneMQ Web-Hooks \nError: %s", err.Error())
	}

	router.Handle("/metrics", promhttp.Handler())

	return &http.Server{
		Addr:    ":8090",
		Handler: router,
	}
}
