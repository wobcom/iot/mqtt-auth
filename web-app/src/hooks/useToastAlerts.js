import { useToasts } from 'react-toast-notifications';

const useToastAlerts = () => {
  return useToasts();
};

export default useToastAlerts;
