package auth

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"io"
	"net/http"
	"time"

	"github.com/MicahParks/keyfunc"
	"github.com/golang-jwt/jwt/v4"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/rs/cors"
	"github.com/rs/zerolog/log"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/config"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/errs"
	"golang.org/x/oauth2"
)

const (
	randStringLen        int           = 16
	CookieMaxAge         time.Duration = 10 * time.Minute
	NegativeCookieMaxAge time.Duration = -1 * CookieMaxAge
	EmptyCookieVal       string        = ""
)

// A private key for context that only this package can access. This is important
// to prevent collisions between different context uses.
var accessCtxKey = &contextKey{"access"} //nolint:gochecknoglobals

type contextKey struct {
	name string
}

// Middleware decodes the Authorization Header and checks it's validity.
//nolint:funlen,gocognit,cyclop
func Middleware(jwks *keyfunc.JWKs, oauthConfig *oauth2.Config) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			accessTokenRaw := getRawTokenFromCookie("access_token", r)
			refreshTokenRaw := getRawTokenFromCookie("refresh_token", r)
			refreshed := false

			reqCtx := context.Background()

			if accessTokenRaw == "" && refreshTokenRaw == "" {
				http.Error(w, errs.ErrMsgTNotFound, http.StatusUnauthorized)

				return
			}

			// Try to refresh access
			if accessTokenRaw == "" {
				var err error

				refreshed = true

				accessTokenRaw, refreshTokenRaw, err = refreshTokens(reqCtx, oauthConfig, refreshTokenRaw)
				if err != nil {
					http.Error(w, err.Error(), http.StatusForbidden)
				}
			}

			claims, err := validateAndGetClaims(jwks, accessTokenRaw)
			if err != nil { //nolint:nestif
				if !refreshed && errors.Is(err, errs.ErrTokenExpired) {
					// Try to refresh access
					accessTokenRaw, refreshTokenRaw, err = refreshTokens(reqCtx, oauthConfig, refreshTokenRaw)
					if err != nil {
						http.Error(w, err.Error(), http.StatusForbidden)
					}

					claims, err = validateAndGetClaims(jwks, accessTokenRaw)
					if err != nil {
						http.Error(w, err.Error(), http.StatusForbidden)
					}
				} else {
					http.Error(w, err.Error(), http.StatusForbidden)
				}
			}

			if refreshed {
				// Set Cookies
				log.Info().Msg("Refreshed tokens")
				acMaxAge, err := getMaxAge(accessTokenRaw)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)

					return
				}

				rfMaxAge, err := getMaxAge(refreshTokenRaw)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)

					return
				}

				setAuthCookie(w, r, "access_token", accessTokenRaw, "/api/query", acMaxAge)
				setAuthCookie(w, r, "refresh_token", refreshTokenRaw, "/api/query", rfMaxAge)
			}

			// put it in context
			ctx := context.WithValue(r.Context(), accessCtxKey, claims)

			expAt := time.Unix(claims.ExpiresAt, 0)
			log.Info().Msgf("Token Expire at %s", expAt)
			// and call the next with our new context
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

func setAuthCookie(w http.ResponseWriter, r *http.Request,
	name, value, path string, maxAge time.Duration) {
	c := &http.Cookie{ //nolint:exhaustivestruct
		Name:     name,
		Value:    value,
		Path:     path,
		MaxAge:   int(maxAge.Seconds()),
		Secure:   r.TLS != nil,
		HttpOnly: true,
	}
	http.SetCookie(w, c)
}

func setIdentityCookie(w http.ResponseWriter, r *http.Request, name, value string, maxAge time.Duration) {
	c := &http.Cookie{ //nolint:exhaustivestruct
		Name:     name,
		Value:    value,
		MaxAge:   int(maxAge.Seconds()),
		Secure:   r.TLS != nil,
		HttpOnly: false,
		Path:     "/",
	}

	http.SetCookie(w, c)
}

func checkState(r *http.Request) error {
	state, err := r.Cookie("state")
	if err != nil {
		return errs.ErrStateNotFound
	}

	if r.URL.Query().Get("state") != state.Value {
		return errs.ErrStateNotMatch
	}

	return nil
}

func SetupJWKS(c config.Config) (*keyfunc.JWKs, error) {
	options := keyfunc.Options{ //nolint:exhaustivestruct
		RefreshErrorHandler: func(err error) {
			log.Printf("There was an error with the jwt.Keyfunc\nError: %s", err.Error())
		},
	}

	// Create the JWKs from the resource at the given URL.
	jwks, err := keyfunc.Get(c.Auth.JWKSUrl, options)
	if err != nil {
		return nil, errors.Wrap(err, errs.ErrMsgJWKSFailed)
	}

	return jwks, nil
}

func SetupCORS(c config.Config, r *mux.Router) error {
	r.Use(cors.New(cors.Options{ //nolint:exhaustivestruct
		AllowedOrigins:   c.Auth.AllowedOrigins,
		AllowedHeaders:   []string{"Authorization", "Content-Type"},
		AllowCredentials: true,
		Debug:            true,
	}).Handler)

	return nil
}

func SetupProtectedRouter(jwks *keyfunc.JWKs, c config.Config, oauthConfig *oauth2.Config, r *mux.Router) error {
	if !c.Auth.Enabled {
		return nil
	}

	r.Use(Middleware(jwks, oauthConfig))

	return nil
}

func validateAndGetClaims(jwks *keyfunc.JWKs, rawToken string) (*AccessTokenClaims, error) {
	var accClaims AccessTokenClaims

	// Parse the JWT.
	accToken, err := jwt.ParseWithClaims(rawToken, &accClaims, jwks.Keyfunc)

	// Check if the token is valid.
	if !accToken.Valid {
		log.Error().Err(err).Msg(errs.ErrMsgTFailed)

		if ve, ok := err.(*jwt.ValidationError); ok { //nolint:errorlint
			switch {
			case ve.Errors&jwt.ValidationErrorMalformed != 0:
				return nil, errs.ErrTokenParseFailed
			case ve.Errors&jwt.ValidationErrorExpired != 0:
				return nil, errs.ErrTokenExpired
			}
		}

		return nil, errs.ErrTokenNotValid
	}

	// log.Info().Msgf("Roles: %s", accClaims.Roles)

	return &accClaims, nil
}

func getRawTokenFromCookie(name string, r *http.Request) string {
	cookie, _ := r.Cookie(name)
	if cookie == nil {
		return ""
	}

	return cookie.Value
}

func randString(nByte int) (string, error) {
	b := make([]byte, nByte)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		return "", errors.Wrap(err, errs.ErrMsgRandString)
	}

	return base64.RawURLEncoding.EncodeToString(b), nil
}

// ForContext finds the access claims from the context. REQUIRES Middleware to have run.
func ForContext(ctx context.Context) *AccessTokenClaims {
	raw, _ := ctx.Value(accessCtxKey).(*AccessTokenClaims)

	return raw
}

func getMaxAge(tokenString string) (time.Duration, error) {
	token, _, err := new(jwt.Parser).ParseUnverified(tokenString, jwt.MapClaims{})
	if err != nil {
		return 0, errors.Wrap(err, errs.ErrMsgTFailed)
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		val, found := claims["exp"]
		if found {
			switch exp := val.(type) {
			case float64:
				return time.Until(time.Unix(int64(exp), 0)).Round(time.Second), nil
			case int64:
				return time.Until(time.Unix(exp, 0)).Round(time.Second), nil
			default:
				return 0, errs.ErrTokenParseFailed
			}
		}
	}

	return 0, errs.ErrTokenParseFailed
}
