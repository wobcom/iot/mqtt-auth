package auth

import (
	"context"
	"net/http"
	"net/url"

	"github.com/coreos/go-oidc/v3/oidc"
	"github.com/golang-jwt/jwt/v4"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/config"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/errs"
	"golang.org/x/oauth2"
)

type TokenTyp string

const (
	BearerTokenTyp  TokenTyp = "Bearer"
	RefreshTokenTyp TokenTyp = "Refresh"
)

type AccessTokenClaims struct {
	jwt.StandardClaims
	Typ           TokenTyp `json:"typ"`
	EmailVerified bool     `json:"email_verified"` //nolint:tagliatelle
	Roles         []string `json:"roles"`
}

func (claims AccessTokenClaims) Valid() error {
	if err := claims.StandardClaims.Valid(); err != nil {
		return errors.Wrap(err, errs.ErrMsgTNotValid)
	}

	if claims.Typ != BearerTokenTyp && !claims.EmailVerified {
		return errs.ErrTokenNotValid
	}

	return nil
}

func (claims AccessTokenClaims) IsViewer() bool {
	return claims.IsAdmin() || claims.hasRole(config.C.Auth.RolesConfig.Viewer)
}

func (claims AccessTokenClaims) IsAdmin() bool {
	return claims.hasRole(config.C.Auth.RolesConfig.Admin)
}

func (claims AccessTokenClaims) hasRole(role string) bool {
	for _, v := range claims.Roles {
		if v == role {
			return true
		}
	}

	return false
}

type RefreshTokenClaims struct {
	jwt.StandardClaims
	Typ TokenTyp `json:"typ"`
}

func NewOAuth(ctx context.Context, c config.Config, provider *oidc.Provider, oauth *oauth2.Config) (*OAuth, error) {
	oidcConfig := &oidc.Config{ //nolint:exhaustivestruct
		ClientID: c.Auth.ClientID,
	}

	cbURL, err := url.Parse(c.Auth.CallbackURL)
	if err != nil {
		return nil, errors.Wrap(err, errs.ErrMsgParseURL)
	}

	scURL, err := url.Parse(c.Auth.LogInSuccessURL)
	if err != nil {
		return nil, errors.Wrap(err, errs.ErrMsgParseURL)
	}

	loURL, err := url.Parse(c.Auth.LogOutURL)
	if err != nil {
		return nil, errors.Wrap(err, errs.ErrMsgParseURL)
	}

	laURL, err := url.Parse(c.Auth.LogOutSuccessURL)
	if err != nil {
		return nil, errors.Wrap(err, errs.ErrMsgParseURL)
	}

	loParams := url.Values{}
	loParams.Add("redirect_uri", laURL.String())
	loURL.RawQuery = loParams.Encode()

	return &OAuth{
		Config:           oauth,
		Verifier:         provider.Verifier(oidcConfig),
		CallbackURL:      cbURL,
		LogInSuccessURL:  scURL,
		LogOutURL:        loURL,
		LogOutSuccessURL: laURL,
	}, nil
}

type OAuth struct {
	Config           *oauth2.Config
	Verifier         *oidc.IDTokenVerifier
	CallbackURL      *url.URL
	LogOutURL        *url.URL
	LogInSuccessURL  *url.URL
	LogOutSuccessURL *url.URL
}

func (oa OAuth) LogInHandler(w http.ResponseWriter, r *http.Request) {
	state, err := randString(randStringLen)
	if err != nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)

		return
	}

	nonce, err := randString(randStringLen)
	if err != nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)

		return
	}

	// jwt.ParseWithClaims()
	setAuthCookie(w, r, "state", state, oa.CallbackURL.Path, CookieMaxAge)
	setAuthCookie(w, r, "nonce", nonce, oa.CallbackURL.Path, CookieMaxAge)

	http.Redirect(w, r, oa.Config.AuthCodeURL(state, oidc.Nonce(nonce)), http.StatusFound)
}

// MaxAge=0 means no 'Max-Age' attribute specified.
// MaxAge<0 means delete cookie now, equivalently 'Max-Age: 0'
// MaxAge>0 means Max-Age attribute present and given in seconds
// Clear ALL Cookies.
func (oa OAuth) LogOutHandler(w http.ResponseWriter, r *http.Request) {
	setIdentityCookie(w, r, "identity", EmptyCookieVal, NegativeCookieMaxAge)
	setAuthCookie(w, r, "state", EmptyCookieVal, oa.CallbackURL.Path, NegativeCookieMaxAge)
	setAuthCookie(w, r, "nonce", EmptyCookieVal, oa.CallbackURL.Path, NegativeCookieMaxAge)
	setAuthCookie(w, r, "access_token", EmptyCookieVal, "/api/query", NegativeCookieMaxAge)
	setAuthCookie(w, r, "refresh_token", EmptyCookieVal, "/api/query", NegativeCookieMaxAge)

	log.Info().Msg(oa.LogOutURL.String())
	http.Redirect(w, r, oa.LogOutURL.String(), http.StatusFound)
}

func (oa OAuth) CallbackHandler(w http.ResponseWriter, r *http.Request) { //nolint:funlen
	ctx := context.Background()

	if err := checkState(r); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)

		return
	}

	oauth2Token, err := oa.Config.Exchange(ctx, r.URL.Query().Get("code"))
	if err != nil {
		http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)

		return
	}

	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		http.Error(w, "No id_token field in oauth2 token.", http.StatusInternalServerError)

		return
	}

	idToken, err := oa.Verifier.Verify(ctx, rawIDToken)
	if err != nil {
		http.Error(w, "Failed to verify ID Token: "+err.Error(), http.StatusInternalServerError)

		return
	}

	nonce, err := r.Cookie("nonce")
	if err != nil {
		http.Error(w, "nonce not found", http.StatusBadRequest)

		return
	}

	if idToken.Nonce != nonce.Value {
		http.Error(w, "nonce did not match", http.StatusBadRequest)

		return
	}

	// Parse the JWT.
	idMaxAge, err := getMaxAge(rawIDToken)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	acMaxAge, err := getMaxAge(oauth2Token.AccessToken)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	rfMaxAge, err := getMaxAge(oauth2Token.RefreshToken)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	log.Info().Msgf("IdToken MaxAge %s", idMaxAge)
	log.Info().Msgf("AccessToken MaxAge %s", acMaxAge)
	log.Info().Msgf("RefreshToken MaxAge %s", rfMaxAge)

	setIdentityCookie(w, r, "identity", rawIDToken, idMaxAge)
	setAuthCookie(w, r, "access_token", oauth2Token.AccessToken, "/api/query", acMaxAge)
	setAuthCookie(w, r, "refresh_token", oauth2Token.RefreshToken, "/api/query", rfMaxAge)

	http.Redirect(w, r, oa.LogInSuccessURL.String(), http.StatusFound)
}

func refreshTokens(ctx context.Context, oauthConfig *oauth2.Config, refreshToken string) (string, string, error) {
	ts := oauthConfig.TokenSource(ctx, &oauth2.Token{RefreshToken: refreshToken}) //nolint:exhaustivestruct

	tok, err := ts.Token()
	if err != nil {
		return "", "", errors.Wrap(err, errs.ErrMsgProvider)
	}

	return tok.AccessToken, tok.RefreshToken, nil
}

// Setup configured the OpenID Connect endpoint handlers.
func Setup(c config.Config, provider *oidc.Provider, oauthConfig *oauth2.Config, r *mux.Router) error {
	if !c.Auth.Enabled {
		return nil
	}

	ctx := context.Background()

	oa, err := NewOAuth(ctx, c, provider, oauthConfig)
	if err != nil {
		return err
	}

	r.HandleFunc("/auth/login", oa.LogInHandler)
	r.HandleFunc("/auth/logout", oa.LogOutHandler)
	r.HandleFunc("/auth/callback", oa.CallbackHandler)

	return nil
}

func NewOAuth2Config(c config.Config, provider *oidc.Provider) *oauth2.Config {
	return &oauth2.Config{
		ClientID:     c.Auth.ClientID,
		ClientSecret: c.Auth.ClientSecret,
		Endpoint:     provider.Endpoint(),
		RedirectURL:  c.Auth.CallbackURL,
		Scopes:       []string{oidc.ScopeOpenID, "profile", "email"},
	}
}

func NetOAuthProvider(ctx context.Context, c config.Config) (*oidc.Provider, error) {
	provider, err := oidc.NewProvider(ctx, c.Auth.IssuerURL)
	if err != nil {
		return nil, errors.Wrap(err, errs.ErrMsgProvider)
	}

	return provider, nil
}
