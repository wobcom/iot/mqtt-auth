//nolint:tagliatelle
package vernemq

import (
	"encoding/json"
	"fmt"

	"github.com/rs/zerolog/log"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/config"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/storage"
)

// SubscribeRequest example
// {
// 	"client_id": "clientid",
// 	"mountpoint": "",
// 	"username": "username",
// 	"topics":
//     	[{"topic": "a/b",
//       	"qos": 1},
//      	{"topic": "c/d",
//       	"qos": 2}]
// }.
type SubscribeRequestM5 struct {
	SubscribeRequest
	Properties map[string]string `json:"properties"`
}

type SubscribeRequest struct {
	ClientID   string      `json:"client_id"`
	Username   string      `json:"username"`
	Mountpoint string      `json:"mountpoint"`
	Topics     []TopicConf `json:"topics"`
}

type SubscribeResponse struct {
	Result string      `json:"result"`
	Topics []TopicConf `json:"topics"`
}

const (
	subscribeQuery string = `
	SELECT subscribe_acl::TEXT 
	FROM vmq_auth_acl 
	WHERE mountpoint=$1 AND username=$2 
	limit 1`

	subscribeStrictQuery string = `
	SELECT subscribe_acl::TEXT 
	FROM vmq_auth_acl 
	WHERE mountpoint=$1 AND client_id=$2 AND username=$3 
	limit 1`
)

func AuthorizeSubscribe(sr SubscribeRequest) (bool, []TopicConf) {
	var ACLRaw string

	var err error

	if config.C.General.StrictMode {
		err = storage.DB().Get(&ACLRaw, subscribeStrictQuery, sr.Mountpoint, sr.ClientID, sr.Username)
	} else {
		err = storage.DB().Get(&ACLRaw, subscribeQuery, sr.Mountpoint, sr.Username)
	}

	if err != nil {
		log.Error().Err(err).Msg("AuthorizeSubscribe")

		return false, nil
	}

	var ACLS []ACL

	err = json.Unmarshal([]byte(ACLRaw), &ACLS)
	if err != nil {
		log.Error().Err(err).Msg("AuthorizeSubscribe")

		return false, nil
	}

	log.Debug().Str("action", "subscribe").Str("clientID", sr.ClientID).Str("username", sr.Username).
		Str("acls", fmt.Sprintf("%s", ACLS)).Send()

	approvedTopics := GetApprovedTopics(ACLS, sr.Topics)

	return len(approvedTopics) > 0, approvedTopics
}

func GetApprovedTopics(acls []ACL, topics []TopicConf) []TopicConf {
	topicsLen := len(topics)
	result := make([]TopicConf, 0, topicsLen)
	rsIdx := 0

	for _, acl := range acls {
		for _, t := range topics {
			if TopicsMatch(acl.Pattern, t.Topic) {
				n := len(result)
				result = result[0 : n+1]
				result[n] = t
			}
		}

		if topicsLen == rsIdx {
			break
		}
	}

	return result
}
