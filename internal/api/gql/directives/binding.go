package directives

import (
	"context"
	"fmt"

	"github.com/99designs/gqlgen/graphql"
	"github.com/go-playground/validator/v10"
)

var validate *validator.Validate //nolint:gochecknoglobals

//nolint:gochecknoinits
func init() {
	validate = validator.New()
}

func Binding(ctx context.Context, obj interface{}, next graphql.Resolver, constraint string) (interface{}, error) {
	val, err := next(ctx)
	if err != nil {
		panic(err)
	}

	fieldName := *graphql.GetPathContext(ctx).Field

	err = validate.Var(val, constraint)
	if err != nil {
		validationErrors := err.(validator.ValidationErrors) //nolint:errorlint,forcetypeassert

		return val, fmt.Errorf("%s%+v", fieldName, validationErrors[0]) //nolint:goerr113
	}

	return val, nil
}
