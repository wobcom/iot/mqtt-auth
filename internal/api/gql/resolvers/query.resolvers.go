//nolint:wrapcheck
package resolver

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/wobcom/iot/mqtt-auth/graph/generated"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/api/auth"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/api/gql/models"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/errs"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/storage"
)

func (r *queryResolver) User(ctx context.Context, userID int) (*models.User, error) {
	if claims := auth.ForContext(ctx); claims == nil || !claims.IsViewer() {
		return nil, errs.ErrNotAuthorized
	}

	return storage.GetUser(userID)
}

func (r *queryResolver) Users(ctx context.Context, filter *models.UserFilter) ([]*models.User, error) {
	if claims := auth.ForContext(ctx); claims == nil || !claims.IsViewer() {
		return nil, errs.ErrNotAuthorized
	}

	return storage.GetUsers(filter)
}

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type queryResolver struct{ *Resolver }
