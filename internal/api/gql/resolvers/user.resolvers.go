package resolver

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/wobcom/iot/mqtt-auth/graph/generated"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/api/gql/models"
)

func (r *userResolver) Subscribe(ctx context.Context, obj *models.User) ([]*models.AccessControlEntry, error) {
	return obj.Subscribe, nil
}

func (r *userResolver) Publish(ctx context.Context, obj *models.User) ([]*models.AccessControlEntry, error) {
	return obj.Publish, nil
}

// User returns generated.UserResolver implementation.
func (r *Resolver) User() generated.UserResolver { return &userResolver{r} }

type userResolver struct{ *Resolver }
