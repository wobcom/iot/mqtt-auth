package gql

import (
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gorilla/mux"
	"gitlab.com/wobcom/iot/mqtt-auth/graph/generated"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/api/gql/directives"
	resolver "gitlab.com/wobcom/iot/mqtt-auth/internal/api/gql/resolvers"
	"gitlab.com/wobcom/iot/mqtt-auth/internal/config"
)

func SetupQuery(c config.Config, r *mux.Router) error {
	gqlConfig := generated.Config{Resolvers: &resolver.Resolver{}} //nolint:exhaustivestruct
	gqlConfig.Directives.Binding = directives.Binding
	gqlSrv := handler.NewDefaultServer(generated.NewExecutableSchema(gqlConfig))
	r.Handle("/api/query", gqlSrv)

	return nil
}

func SetupPlayground(c config.Config, r *mux.Router) error {
	if !config.C.General.PlaygroundEnabled {
		return nil
	}

	r.Handle("/api/playground", playground.Handler("GraphQL playground", "/api/query"))

	return nil
}
