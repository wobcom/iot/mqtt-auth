package models

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
)

type AccessControlEntry struct {
	Pattern string `json:"pattern"`
	Qos     int    `json:"qos"`
}

type ACL []*AccessControlEntry

//nolint:wrapcheck
func (acl *ACL) Scan(val interface{}) error {
	var err error

	switch v := val.(type) {
	case nil:
		err = json.Unmarshal([]byte("[]"), &acl)
	case []byte:
		err = json.Unmarshal(v, &acl)
	case string:
		err = json.Unmarshal([]byte(v), &acl)
	default:
		return fmt.Errorf("unsupported type: %T", v) //nolint:goerr113
	}

	return err
}

//nolint:wrapcheck
func (acl ACL) Value() (driver.Value, error) {
	return json.Marshal(acl)
}

type ACLInput []AccessControlEntryInput

//nolint:wrapcheck
func (ace ACLInput) Value() (driver.Value, error) {
	return json.Marshal(ace)
}

type ACLInputPtr []*AccessControlEntryInput

//nolint:wrapcheck
func (ace ACLInputPtr) Value() (driver.Value, error) {
	return json.Marshal(ace)
}

type AccessControlEntryInput struct {
	Pattern string `json:"pattern"`
	Qos     int    `json:"qos"`
}

//nolint:wrapcheck
func (ace AccessControlEntryInput) Value() (driver.Value, error) {
	return json.Marshal(ace)
}
