package errs

import (
	"errors"
	"fmt"
)

const (
	ErrMsgTMalformed    string = "bearer token not in proper format"
	ErrMsgTFailed       string = "failed to parse the JWT"
	ErrMsgTNotValid     string = "token is not valid"
	ErrMsgTExpired      string = "token has expired"
	ErrMsgTNotFound     string = "token not found"
	ErrMsgStateNotFound string = "state not found"
	ErrMsgStateNotMatch string = "state did not match"
	ErrMsgParseURL      string = "failed to parse the URL"
	ErrMsgProvider      string = "failed to create new OIDC Provider"
	ErrMsgRandString    string = "failed to create new random string"
	ErrMsgJWKSFailed    string = "failed to create new JWKS"
	ErrMsgNotAuthorized string = "not authorized"
)

var (
	// ErrMissingConfigValue is used to error on missing config values.
	ErrMissingConfigValue = errors.New("missing config value")
	// ErrLoadingConfigFile is used to error on loading config file.
	ErrLoadingConfigFile = errors.New("error loading config file")
	// ErrDBNotAvailable is a error raised if database is not available.
	ErrDBNotAvailable   = errors.New("database not available")
	ErrStateNotFound    = errors.New(ErrMsgStateNotFound)
	ErrStateNotMatch    = errors.New(ErrMsgStateNotMatch)
	ErrParseURL         = errors.New(ErrMsgParseURL)
	ErrNotAuthorized    = errors.New(ErrMsgNotAuthorized)
	ErrTokenNotValid    = errors.New(ErrMsgTNotValid)
	ErrTokenParseFailed = errors.New(ErrMsgTFailed)
	ErrTokenExpired     = errors.New(ErrMsgTExpired)
	ErrTokenNotFound    = errors.New(ErrMsgTNotFound)
)

func MissingConfigError(msg string) error {
	return fmt.Errorf("%w: %s", ErrMissingConfigValue, msg)
}
