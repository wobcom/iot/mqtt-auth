# mqtt-auth

![Version: 0.5.0](https://img.shields.io/badge/Version-0.5.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: v0.7.0](https://img.shields.io/badge/AppVersion-v0.7.0-informational?style=flat-square)

A Helm chart providing an authentication/authorization backend for MQTT-Brokers

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://vernemq.github.io/docker-vernemq | vernemq | v1.6.3 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| fullnameOverride | string | `"mqtt-auth"` | This need to be set -- If you change it here, remember to change it in vernemq.additionalEnv as well! |
| global.vernemq.enabled | bool | `true` |  |
| mqtt.backend.affinity | object | `{}` |  |
| mqtt.backend.autoscaling.enabled | bool | `false` |  |
| mqtt.backend.autoscaling.maxReplicas | int | `10` |  |
| mqtt.backend.autoscaling.minReplicas | int | `1` |  |
| mqtt.backend.autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| mqtt.backend.configuration.auth.callbackURL | string | `"https://external.domain.example/auth/callback"` | Has to be accessible from the OIDC Server |
| mqtt.backend.configuration.auth.clientID | string | `"MY_OIDC_CLIENT"` |  |
| mqtt.backend.configuration.auth.clientSecret | string | `"MY_OIDC_CLIENT_SECRET"` |  |
| mqtt.backend.configuration.auth.cors.allowedOrigins[0] | string | `"http://localhost:3000"` |  |
| mqtt.backend.configuration.auth.cors.allowedOrigins[1] | string | `"http://localhost:8080"` |  |
| mqtt.backend.configuration.auth.cors.allowedOrigins[2] | string | `"http://localhost:8000"` |  |
| mqtt.backend.configuration.auth.enable | bool | `true` |  |
| mqtt.backend.configuration.auth.existingSecret.clientIDKey | string | `"clientID"` |  |
| mqtt.backend.configuration.auth.existingSecret.clientSecretKey | string | `"clientSecret"` |  |
| mqtt.backend.configuration.auth.existingSecret.name | string | `"oidc-credentials-mqtt-auth"` |  |
| mqtt.backend.configuration.auth.existingSecret.use | bool | `false` |  |
| mqtt.backend.configuration.auth.issuerURL | string | `"https://localhost:8000/auth/realms/MY_REALM"` |  |
| mqtt.backend.configuration.auth.jwksURL | string | `"https://localhost:8000/auth/realms/MY_REALM/protocol/openid-connect/certs"` |  |
| mqtt.backend.configuration.auth.logoutURL | string | `"https://localhost:8000/auth/realms/MY_REALM/protocol/openid-connect/logout"` |  |
| mqtt.backend.configuration.auth.redirect.afterLogin | string | `"https://external.domain.example/api/playground"` |  |
| mqtt.backend.configuration.auth.redirect.afterLogout | string | `"https://external.domain.example/api/playground"` |  |
| mqtt.backend.configuration.auth.rolesMapping.admin | string | `"mqtt_admin_full"` | Mapping between OIDC-Roles and mqtt-auth |
| mqtt.backend.configuration.auth.rolesMapping.viewer | string | `"mqtt_admin_simple"` | Mapping between OIDC-Roles and mqtt-auth |
| mqtt.backend.configuration.general.checkClientId | bool | `false` |  |
| mqtt.backend.configuration.general.enableGraphQLPlayground | bool | `false` |  |
| mqtt.backend.configuration.general.enableMQTTv5 | bool | `false` |  |
| mqtt.backend.configuration.general.logLevel | int | `4` | debug=5, info=4, warning=3, error=2, fatal=1, panic=0  |
| mqtt.backend.configuration.postgres.autoMigrate | bool | `false` |  |
| mqtt.backend.configuration.postgres.connectTimeout | int | `5` |  |
| mqtt.backend.configuration.postgres.dbName | string | `"vernemq_db"` |  |
| mqtt.backend.configuration.postgres.existingSecret.name | string | `"vernemq.postgres.credentials.postgresql.acid.zalan.do"` |  |
| mqtt.backend.configuration.postgres.existingSecret.passwordKey | string | `"password"` |  |
| mqtt.backend.configuration.postgres.existingSecret.use | bool | `false` |  |
| mqtt.backend.configuration.postgres.existingSecret.usernameKey | string | `"username"` |  |
| mqtt.backend.configuration.postgres.host | string | `"postgres.default.svc.cluster.local"` |  |
| mqtt.backend.configuration.postgres.password | string | `"mySecurePassword"` |  |
| mqtt.backend.configuration.postgres.sslMode | string | `"require"` |  |
| mqtt.backend.configuration.postgres.username | string | `"auth-db"` |  |
| mqtt.backend.image.pullPolicy | string | `"IfNotPresent"` |  |
| mqtt.backend.image.repository | string | `"harbor.service.wobcom.de/public/mqtt-auth"` |  |
| mqtt.backend.image.tag | string | `"v0.7.0"` | Overrides the image tag whose default is the chart appVersion. |
| mqtt.backend.imagePullSecrets | list | `[]` |  |
| mqtt.backend.nodeSelector | object | `{}` |  |
| mqtt.backend.podAnnotations | object | `{}` |  |
| mqtt.backend.podSecurityContext | object | `{}` |  |
| mqtt.backend.replicaCount | int | `3` |  |
| mqtt.backend.resources | object | `{}` |  |
| mqtt.backend.securityContext | object | `{}` |  |
| mqtt.backend.service.external.port | int | `80` |  |
| mqtt.backend.service.external.type | string | `"ClusterIP"` |  |
| mqtt.backend.service.internal.port | int | `80` |  |
| mqtt.backend.service.internal.type | string | `"ClusterIP"` |  |
| mqtt.backend.serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| mqtt.backend.serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| mqtt.backend.serviceAccount.name | string | `""` | The name of the service account to use. -- If not set and create is true, a name is generated using the fullname template |
| mqtt.backend.sidecars | list | `[]` |  |
| mqtt.backend.tolerations | list | `[]` |  |
| mqtt.frontend.affinity | object | `{}` |  |
| mqtt.frontend.autoscaling.enabled | bool | `false` |  |
| mqtt.frontend.autoscaling.maxReplicas | int | `10` |  |
| mqtt.frontend.autoscaling.minReplicas | int | `1` |  |
| mqtt.frontend.autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| mqtt.frontend.image.pullPolicy | string | `"IfNotPresent"` |  |
| mqtt.frontend.image.repository | string | `"harbor.service.wobcom.de/public/mqtt-auth-ui"` |  |
| mqtt.frontend.image.tag | string | `"v0.7.0"` | Overrides the image tag whose default is the chart appVersion. |
| mqtt.frontend.imagePullSecrets | list | `[]` |  |
| mqtt.frontend.nodeSelector | object | `{}` |  |
| mqtt.frontend.podAnnotations | object | `{}` |  |
| mqtt.frontend.podSecurityContext | object | `{}` |  |
| mqtt.frontend.replicaCount | int | `3` |  |
| mqtt.frontend.resources | object | `{}` |  |
| mqtt.frontend.securityContext | object | `{}` |  |
| mqtt.frontend.service.port | int | `80` |  |
| mqtt.frontend.service.type | string | `"ClusterIP"` |  |
| mqtt.frontend.sidecars | list | `[]` |  |
| mqtt.frontend.tolerations | list | `[]` |  |
| mqtt.ingress.annotations | object | `{}` |  |
| mqtt.ingress.enabled | bool | `false` |  |
| mqtt.ingress.host | string | `"auth.vernemq.domain.com"` |  |
| mqtt.ingress.labels | object | `{}` |  |
| mqtt.ingress.tls.enabled | bool | `true` |  |
| mqtt.ingress.tls.secretName | string | `"mqtt-auth-cert"` |  |
| nameOverride | string | `"mqtt-auth"` |  |
| vernemq.additionalEnv | list | `[{"name":"DOCKER_VERNEMQ_ALLOW_ANONYMOUS","value":"off"},{"name":"DOCKER_VERNEMQ_plugins.vmq_diversity","value":"off"},{"name":"DOCKER_VERNEMQ_plugins.vmq_passwd","value":"off"},{"name":"DOCKER_VERNEMQ_plugins.vmq_acl","value":"off"},{"name":"DOCKER_VERNEMQ_plugins.vmq_webhooks","value":"on"},{"name":"DOCKER_VERNEMQ_vmq_webhooks.authOnRegister.hook","value":"auth_on_register"},{"name":"DOCKER_VERNEMQ_vmq_webhooks.authOnRegister.endpoint","value":"http://mqtt-auth/vernemq/auth_on_register"},{"name":"DOCKER_VERNEMQ_vmq_webhooks.authOnSubscribe.hook","value":"auth_on_subscribe"},{"name":"DOCKER_VERNEMQ_vmq_webhooks.authOnSubscribe.endpoint","value":"http://mqtt-auth/vernemq/auth_on_subscribe"},{"name":"DOCKER_VERNEMQ_vmq_webhooks.authOnPublish.hook","value":"auth_on_publish"},{"name":"DOCKER_VERNEMQ_vmq_webhooks.authOnPublish.endpoint","value":"http://mqtt-auth/vernemq/auth_on_publish"}]` | Ensure the service name equals the fullnameOverride you've set before! |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.5.0](https://github.com/norwoodj/helm-docs/releases/v1.5.0)
