{{- define "mqtt-auth.backend.labels" -}}
{{ include "mqtt-auth.labels" . }}
{{ include "mqtt-auth.backend.selectorLabels" . }}
{{- end }}


{{/*
Selector labels
*/}}
{{- define "mqtt-auth.backend.selectorLabels" -}}
app.kubernetes.io/name: {{ include "mqtt-auth.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "mqtt-auth.backend.serviceAccountName" -}}
{{- if .Values.mqtt.backend.serviceAccount.create }}
{{- default (include "mqtt-auth.fullname" .) .Values.mqtt.backend.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.mqtt.backend.serviceAccount.name }}
{{- end }}
{{- end }}


{{- define "mqtt-auth.backend.postgres.dsn" -}}
{{- printf "user=$(POSTGRESQL__USER) password=$(POSTGRESQL__PASSWORD) host=%s dbname=%s sslmode=%s connect_timeout=%d"  .Values.mqtt.backend.configuration.postgres.host .Values.mqtt.backend.configuration.postgres.dbName .Values.mqtt.backend.configuration.postgres.sslMode (.Values.mqtt.backend.configuration.postgres.connectTimeout | int) -}}
{{- end -}}
