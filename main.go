package main

import (
	"gitlab.com/wobcom/iot/mqtt-auth/cmd"
)

func main() {
	cmd.Execute()
}
