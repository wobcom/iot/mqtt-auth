[general]
# Log level
#
# debug=5, info=4, warning=3, error=2, fatal=1, panic=0
log_level={{ .General.LogLevel }}

shutdown_timeout="{{ .General.ShutdownTimeout}}"

strict_mode = true

enable_mqtt_v5 ="{{ .General.M5Enabled }}" 

# This enables the GraphQL Playground
enable_playground="{{ .General.PlaygroundEnabled }}"

[auth]

# This adds an auth middleware to the GraphQL Endpoint
enable="{{ .Auth.Enabled }}

# Get the JWKs URL from your JWT Provider
# Example (Keycloak): https://localhost:8000/auth/realms/MY_REALM/protocol/openid-connect/certs
jwks_url="{{ .Auth.JWKSUrl }}"

# Example (Keycloak): http://localhost:8000/auth/realms/MY_REALM
issuer_url="{{ .Auth.IssuerURL }}"

# Example (Keycloak): http://localhost:8000/auth/realms/MY_REALM/protocol/openid-connect/logout
logout_url="{{ .Auth.LogOutURL}}"

# The external accessible URL to callback after login flow (needs to be allowed at the provider)
callback_url="{{ .Auth.CallbackURL }}"

# The external accessible URL to redirect after successful login
login_success_url="{{ .Auth.LogInSuccessURL }}"

The external accessible URL to callback after logout flow (needs to be allowed at the provider)
logout_success_url="{{ .Auth.LogOutSuccessURL}}"

# OIDC Client ID
client_id="{{ .Auth.ClientID }}"

# OIDC Client Secret
client_secret="{{ .Auth.ClientSecret }}"

[auth.roles]
viewer="{{.Auth.Roles.Viewer}}"

admin="{{.Auth.Roles.Admin}}"

# PostgreSQL settings.
#
# Please note that PostgreSQL 9.5+ is required.
[postgresql]
# PostgreSQL dsn (e.g.: postgres://user:password@hostname/database?sslmode=disable).
#
# Besides using an URL (e.g. 'postgres://user:password@hostname/database?sslmode=disable')
# it is also possible to use the following format:
# 'user=vernemq dbname=vernemq_db sslmode=disable'.
#
# The following connection parameters are supported:
#
# * dbname - The name of the database to connect to
# * user - The user to sign in as
# * password - The user's password
# * host - The host to connect to. Values that start with / are for unix domain sockets. (default is localhost)
# * port - The port to bind to. (default is 5432)
# * sslmode - Whether or not to use SSL (default is require, this is not the default for libpq)
# * fallback_application_name - An application_name to fall back to if one isn't provided.
# * connect_timeout - Maximum wait for connection, in seconds. Zero or not specified means wait indefinitely.
# * sslcert - Cert file location. The file must contain PEM encoded data.
# * sslkey - Key file location. The file must contain PEM encoded data.
# * sslrootcert - The location of the root certificate file. The file must contain PEM encoded data.
#
# Valid values for sslmode are:
#
# * disable - No SSL
# * require - Always SSL (skip verification)
# * verify-ca - Always SSL (verify that the certificate presented by the server was signed by a trusted CA)
# * verify-full - Always SSL (verify that the certification presented by the server was signed by a trusted CA and the server host name matches the one in the certificate)
dsn="{{ .PostgreSQL.DSN }}"

# Max open connections.
#
# This sets the max. number of open connections that are allowed in the
# PostgreSQL connection pool (0 = unlimited).
max_open_connections={{ .PostgreSQL.MaxOpenConnections }}

# Max idle connections.
#
# This sets the max. number of idle connections in the PostgreSQL connection
# pool (0 = no idle connections are retained).
max_idle_connections={{ .PostgreSQL.MaxIdleConnections }}

max_conn_lifetime="{{ .PostgreSQL.ConnMaxLifetime }}"

auto_migrate="{{ .PostgreSQL.AutoMigrate }}"
